package kpmg.trungkenbi;

public class KinematicSeek {
	Kinematic character;
	Kinematic target;
	float maxSpeed;

	public KinematicSeek(Kinematic charater, Kinematic target, float maxSpeed) {
		this.character = charater;
		this.target = target;
		this.maxSpeed = maxSpeed;
	}

	public Kinematic getCharater() {
		return character;
	}

	public Kinematic getTarget() {
		return target;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public void setCharater(Kinematic charater) {
		this.character = charater;
	}

	public void setTarget(Kinematic target) {
		this.target = target;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((character == null) ? 0 : character.hashCode());
		result = prime * result + Float.floatToIntBits(maxSpeed);
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KinematicSeek other = (KinematicSeek) obj;
		if (character == null) {
			if (other.character != null)
				return false;
		} else if (!character.equals(other.character))
			return false;
		if (Float.floatToIntBits(maxSpeed) != Float.floatToIntBits(other.maxSpeed))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "KinematicSeek{" + "charater=" + character + ", target=" + target + ", maxSpeed=" + maxSpeed + '}';
	}

	public KinematicOutput generateKinematicOutput() {
		Vector2D velocity = new Vector2D();
		velocity = character.getPosition().subVector2D(target.getPosition());
		velocity.normalize();
		velocity.mulConstant(maxSpeed);
		return new KinematicOutput(velocity, 0);
	}
}