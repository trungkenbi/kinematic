package kpmg.trungkenbi;

public class KinematicOutput {
	private Vector2D velocity;
	private float rotation;

	public KinematicOutput(Vector2D velocity, float rotation) {
		this.velocity = velocity;
		this.rotation = rotation;
	}

	public void setVelocity(Vector2D velocity) {
		this.velocity = velocity;
	}

	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	public Vector2D getVelocity() {
		return velocity;
	}

	public float getRotation() {
		return rotation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(rotation);
		result = prime * result + ((velocity == null) ? 0 : velocity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KinematicOutput other = (KinematicOutput) obj;
		if (Float.floatToIntBits(rotation) != Float.floatToIntBits(other.rotation))
			return false;
		if (velocity == null) {
			if (other.velocity != null)
				return false;
		} else if (!velocity.equals(other.velocity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "KinematicOutput{" + "velocity=" + velocity + ", rotation=" + rotation + '}';
	}
}