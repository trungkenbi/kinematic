package kpmg.trungkenbi;

public class Kinematic {
	private Vector2D position;
	private float orientation;
	private Vector2D velocity;
	private float rotation;

	public Kinematic(Vector2D position, float orientation, Vector2D velocity, float rotation) {
		this.position = position;
		this.orientation = orientation;
		this.velocity = velocity;
		this.rotation = rotation;
	}

	public Vector2D getPosition() {
		return position;
	}

	public float getOrientation() {
		return orientation;
	}

	public Vector2D getVelocity() {
		return velocity;
	}

	public float getRotation() {
		return rotation;
	}

	public void setPosition(Vector2D position) {
		this.position = position;
	}

	public void setOrientation(float orientation) {
		this.orientation = orientation;
	}

	public void setVelocity(Vector2D velocity) {
		this.velocity = velocity;
	}

	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(orientation);
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		result = prime * result + Float.floatToIntBits(rotation);
		result = prime * result + ((velocity == null) ? 0 : velocity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kinematic other = (Kinematic) obj;
		if (Float.floatToIntBits(orientation) != Float.floatToIntBits(other.orientation))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (Float.floatToIntBits(rotation) != Float.floatToIntBits(other.rotation))
			return false;
		if (velocity == null) {
			if (other.velocity != null)
				return false;
		} else if (!velocity.equals(other.velocity))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Character{" + "position=" + position + ", orientation=" + orientation + ", velocity=" + velocity
				+ ", rotation=" + rotation + '}';
	}

	public void update(KinematicOutput kinematicOutput, float time) {
		this.velocity = kinematicOutput.getVelocity();
		this.rotation = kinematicOutput.getRotation();

		this.position.addVector2D(this.velocity.mulConstant(time));
		this.orientation += this.rotation * time;
	}

	public void applyNewOrientation() {
		if (this.velocity.length() > 0) {
			this.orientation = (float) Math.atan2(-this.velocity.getX(), this.rotation);
		}
	}
}